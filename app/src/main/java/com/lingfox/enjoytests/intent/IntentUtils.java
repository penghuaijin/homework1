package com.lingfox.enjoytests.intent;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;

public class IntentUtils {

    private static HashMap<String, Object> map = new HashMap<>();

    public static void initIntent(Activity activity, Class endActivity) {

        map.clear();

        Class<? extends Activity> cls = activity.getClass();
        Field[] declaredFields = cls.getDeclaredFields();

        Intent intent = new Intent(activity, endActivity);

        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(IntentFiled.class)) {

                IntentFiled annotation = field.getAnnotation(IntentFiled.class);
                assert annotation != null;
                field.setAccessible(true);
                Class<?> type = field.getType();
                String type2 = field.getType().getName();
                try {

                    boolean isUsed=false;
                    if (type == String.class) {
                        intent.putExtra(field.getName(), String.valueOf(field.get(activity)));
                        map.put(field.getName(), String.valueOf(field.get(activity)));
                        isUsed=true;
                    }
                    if (type == Short.class||type2.equals("short")) {
                        intent.putExtra(field.getName(), field.getShort(activity));
                        map.put(field.getName(), field.getShort(activity));
                        isUsed=true;
                    }
                    if (type == Integer.class||type2.equals("int")) {
                        intent.putExtra(field.getName(), field.getInt(activity));
                        map.put(field.getName(), field.getInt(activity));
                        isUsed=true;
                    }
                    if (type == Long.class||type2.equals("long")) {
                        intent.putExtra(field.getName(), field.getLong(activity));
                        map.put(field.getName(), field.getLong(activity));
                        isUsed=true;
                    }
                    if (type == Float.class||type2.equals("float")) {
                        intent.putExtra(field.getName(), field.getFloat(activity));
                        map.put(field.getName(), field.getFloat(activity));
                        isUsed=true;
                    }
                    if (type == Double.class||type2.equals("double")) {
                        intent.putExtra(field.getName(), field.getDouble(activity));
                        map.put(field.getName(), field.getDouble(activity));
                        isUsed=true;
                    }
                    if (type == Boolean.class||type2.equals("boolean")) {
                        intent.putExtra(field.getName(), field.getBoolean(activity));
                        map.put(field.getName(), field.getBoolean(activity));
                        isUsed=true;
                    }
                    if (type == Byte.class||type2.equals("byte")) {
                        intent.putExtra(field.getName(), field.getByte(activity));
                        map.put(field.getName(), field.getByte(activity));
                        isUsed=true;
                    }
                    if (type == Character.class||type2.equals("char")) {
                        intent.putExtra(field.getName(), field.getChar(activity));
                        map.put(field.getName(), field.getChar(activity));
                        isUsed=true;
                    }else if (!isUsed){
                        Toast.makeText(activity, String.format("暂时不支持%s类型",type), Toast.LENGTH_SHORT).show();
                    }
//                    if (type == Serializable.class) {
//                        intent.putExtra(field.getName(), new Gson().toJson(field.get(activity)));
//                        map.put(field.getName(), new Gson().toJson(field.get(activity)));
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }
        activity.startActivity(intent);


    }

    public static HashMap getIntent() {
        return map;
    }
}
