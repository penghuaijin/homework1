package com.lingfox.enjoytests;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.lingfox.enjoytests.inject.InjectUtils;
import com.lingfox.enjoytests.inject.InjectView;
import com.lingfox.enjoytests.intent.IntentFiled;
import com.lingfox.enjoytests.intent.IntentUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    @InjectView(R.id.tv_text)
    TextView textView;

    @IntentFiled()
    int age=1;
    @IntentFiled()
    String name="tom";

    @IntentFiled
    ArrayList<String> list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InjectUtils.injectView(this);


        textView.setText("点我");
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.initIntent(MainActivity.this,SecondActivity.class);
            }
        });
    }
}
