package com.lingfox.enjoytests.inject;

import androidx.annotation.IdRes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface InjectView {

    @IdRes int value();
}
