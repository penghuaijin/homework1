package com.lingfox.enjoytests.inject;

import android.app.Activity;
import android.view.View;

import java.lang.reflect.Field;

public class InjectUtils {

    public static void injectView(Activity activity) {
        Class<? extends Activity> cls = activity.getClass();

        //获取此类所有的成员
        Field[] declaredFields = cls.getDeclaredFields();


        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(InjectView.class)) {
                InjectView injectView=field.getAnnotation(InjectView.class);
                assert injectView != null;
                int id = injectView.value();
                View view = activity.findViewById(id);
                //反射设置属性的值

                field.setAccessible(true);
                try {
                    //反射赋值
                    field.set(activity,view);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
