package com.lingfox.enjoytests;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.lingfox.enjoytests.inject.InjectUtils;
import com.lingfox.enjoytests.inject.InjectView;
import com.lingfox.enjoytests.intent.IntentUtils;

import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {

    @InjectView(R.id.tv_text)
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        InjectUtils.injectView(this);
        HashMap map = IntentUtils.getIntent();
        for (Object o : map.keySet()) {
            String key = String.valueOf(o);

            textView.setText(String.format("%s\n%s\t%s", textView.getText()+" :", key, map.get(key).toString()));
        }
    }
}
